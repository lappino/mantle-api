#!/bin/bash

################################################################################
# Copyright (c) 2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script configures cmake
################################################################################

# joins arguments using the cmake list separator (;)
function join_paths()
{
  local IFS=\;
  echo "$*"
}

MYDIR="$(dirname "$(readlink -f $0)")"
cd "$MYDIR/../../../../build" || exit 1

# preparations for building on Windows/MSYS
if [[ "${OSTYPE}" = "msys" ]]; then
  # set the correct CMake generator
  CMAKE_GENERATOR_ARG="-GMSYS Makefiles"

fi

cmake \
  "$CMAKE_GENERATOR_ARG" \
  -D CMAKE_INSTALL_PREFIX="$PWD/../dist/mantle-api" \
  -D CMAKE_BUILD_TYPE=Release \
  ../repo

