################################################################################
# Copyright (c) 2021 Daimler TSS GmbH
# Copyright (c) 2022 Mercedes-Benz Tech Innovation GmbH
#
# This program and the accompanying materials are made available under the terms
# of the Eclipse Public License 2.0 which is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

cmake_minimum_required(VERSION 3.15.0 FATAL_ERROR)

# Add the custom CMake modules to CMake's module path
list(PREPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake")

# Determine if MantleAPI is built as a subproject (using add_subdirectory) or if it is the main project.
if(NOT DEFINED MantleAPI_MAIN_PROJECT)
  set(MantleAPI_MAIN_PROJECT OFF)
  if(CMAKE_CURRENT_SOURCE_DIR STREQUAL CMAKE_SOURCE_DIR)
    set(MantleAPI_MAIN_PROJECT ON)
  endif()
endif()

project(
  MantleAPI
  VERSION 0.1.0
  DESCRIPTION "MantleAPI, an abstraction layer for environmental simulators"
  LANGUAGES CXX
)

# Options that control generation of various targets.
option(MantleAPI_DOC "Generate the doc target." ${MantleAPI_MAIN_PROJECT})
option(MantleAPI_INSTALL "Generate the install target." ${MantleAPI_MAIN_PROJECT})
option(MantleAPI_TEST "Generate the test target." ${MantleAPI_MAIN_PROJECT})
option(MantleAPI_PACKAGE "Generate the package target." ${MantleAPI_MAIN_PROJECT})
option(MantleAPI_ENABLE_WARNINGS "Enable compiler warnings." ${MantleAPI_MAIN_PROJECT})
option(MantleAPI_ENABLE_WERROR "Fail and stop if a warning is triggered." ${MantleAPI_MAIN_PROJECT})

include(CMakeDependentOption)
cmake_dependent_option(MantleAPI_INSTALL_MOCKS "Install the mocks." OFF "MantleAPI_INSTALL;MantleAPI_TEST" ON)

if(MSVC)
  if(MantleAPI_ENABLE_WARNINGS)
    add_compile_options(/W4)
  endif()
  if(MantleAPI_ENABLE_WERROR)
    add_compile_options(/WX /wd4996)
  endif()
else()
  if(MantleAPI_ENABLE_WARNINGS)
    add_compile_options(-Wall -Wextra)
  endif()
  if(MantleAPI_ENABLE_WERROR)
    add_compile_options(-Werror -Wno-error=deprecated-declarations)
  endif()
endif()

# Fetch dependencies
include(CPM)
CPMAddPackage(
  NAME units
  GITHUB_REPOSITORY nholthaus/units
  GIT_TAG v2.3.3
  VERSION 2.3.3
  PATCH_COMMAND git apply ${CMAKE_CURRENT_LIST_DIR}/patches/units/pascal-name-conflict.patch || true
  OPTIONS "DISABLE_PREDEFINED_UNITS ON"
          "ENABLE_PREDEFINED_ACCELERATION_UNITS ON"
          "ENABLE_PREDEFINED_ANGLE_UNITS ON"
          "ENABLE_PREDEFINED_ANGULAR_VELOCITY_UNITS ON"
          "ENABLE_PREDEFINED_CONCENTRATION_UNITS ON"
          "ENABLE_PREDEFINED_FORCE_UNITS ON"
          "ENABLE_PREDEFINED_LENGTH_UNITS ON"
          "ENABLE_PREDEFINED_MASS_UNITS ON"
          "ENABLE_PREDEFINED_PRESSURE_UNITS ON"
          "ENABLE_PREDEFINED_TEMPERATURE_UNITS ON"
          "ENABLE_PREDEFINED_TIME_UNITS ON"
          "ENABLE_PREDEFINED_VELOCITY_UNITS ON"
)

# Add library
add_library(MantleAPI INTERFACE)
add_library(MantleAPI::MantleAPI ALIAS MantleAPI)

target_link_libraries(MantleAPI INTERFACE units::units)

include(GNUInstallDirs)
set(INSTALL_CONFIG_DIR "${CMAKE_INSTALL_LIBDIR}/cmake/MantleAPI")

target_include_directories(
  MantleAPI INTERFACE $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include> $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
)

target_compile_features(MantleAPI INTERFACE cxx_std_17)

# Build docs
if(MantleAPI_DOC)
  add_subdirectory(doc)
endif()

# Build tests
if(MantleAPI_TEST)
  file(
    GLOB_RECURSE INTERFACE_HEADER_SET
    RELATIVE "${PROJECT_SOURCE_DIR}/include"
    CONFIGURE_DEPENDS "include/MantleAPI/**/*.h"
  )
  enable_testing()
  add_subdirectory(test)
endif()

# Configure export
include(CMakePackageConfigHelpers)
configure_package_config_file(
  "${PROJECT_SOURCE_DIR}/cmake/MantleAPIConfig.cmake.in" "${PROJECT_BINARY_DIR}/MantleAPIConfig.cmake"
  INSTALL_DESTINATION ${INSTALL_CONFIG_DIR}
  PATH_VARS CMAKE_INSTALL_INCLUDEDIR
)

write_basic_package_version_file(
  "${PROJECT_BINARY_DIR}/MantleAPIConfigVersion.cmake"
  VERSION ${PROJECT_VERSION}
  COMPATIBILITY SameMajorVersion
)

# Configure install
if(MantleAPI_INSTALL)
  install(DIRECTORY include/ TYPE INCLUDE)
  install(TARGETS MantleAPI EXPORT MantleAPITargets)

  install(
    EXPORT MantleAPITargets
    DESTINATION ${INSTALL_CONFIG_DIR}
    NAMESPACE MantleAPI::
  )

  install(
    FILES "${PROJECT_BINARY_DIR}/MantleAPIConfig.cmake" "${PROJECT_BINARY_DIR}/MantleAPIConfigVersion.cmake"
    DESTINATION ${INSTALL_CONFIG_DIR}
    COMPONENT dev
  )
endif()

# Configure package
if(MantleAPI_PACKAGE)
  include(MantleAPICPack)
endif()
