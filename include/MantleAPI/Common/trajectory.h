/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  trajectory.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_TRAJECTORY_H
#define MANTLEAPI_COMMON_TRAJECTORY_H

#include <MantleAPI/Common/poly_line.h>

#include <string>
#include <variant>

namespace mantle_api
{
/// Definition of a trajectory type in terms of shape
struct Trajectory
{
  std::string name;             ///< Name of the trajectory type
  std::variant<PolyLine> type;  ///< Trajectory type in terms of shape

  /// @brief Prints a human-readable representation of 'trajectory' to 'os'.
  /// @param os         The output stream
  /// @param trajectory Open scenario trajectory
  /// @returns 'trajectory' in a human-readable format
  friend std::ostream& operator<<(std::ostream& os, const Trajectory& trajectory);
};

/// @brief Prints a human-readable representation of 'trajectory' to 'os'.
/// @param os         The output stream
/// @param trajectory Open scenario trajectory
/// @returns 'trajectory' in a human-readable format
inline std::ostream& operator<<(std::ostream& os, const Trajectory& trajectory)
{
  os << "Trajectory \"" << trajectory.name;

  if (std::holds_alternative<PolyLine>(trajectory.type))
  {
    const auto& polyLine = std::get<PolyLine>(trajectory.type);
    for (const auto& polyLinePoint : polyLine)
    {
      os << polyLinePoint;
    }
  }

  os << "\"\n";

  return os;
}

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_TRAJECTORY_H