/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_identifiable.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_I_IDENTIFIABLE_H
#define MANTLEAPI_COMMON_I_IDENTIFIABLE_H

#include <cstdint>
#include <limits>
#include <string>

namespace mantle_api
{
using UniqueId = std::uint64_t;                                      ///< Container for unique id
constexpr UniqueId InvalidId{std::numeric_limits<UniqueId>::max()};  ///< 'invalid' id

/// Common interface for all classes that can be referenced by an ID or name.
class IIdentifiable
{
public:
  virtual ~IIdentifiable() = default;

  /// @brief The unique id is provided and maintained by the scenario simulator.
  /// @return Unique id
  [[nodiscard]] virtual UniqueId GetUniqueId() const = 0;

  /// @brief The scenario description is responsible for keeping the name unique.
  /// @param name Scenario specific name of an object
  virtual void SetName(const std::string& name) = 0;

  /// @brief The scenario description is responsible for keeping the name unique.
  /// @return Scenario specific name of an object
  [[nodiscard]] virtual const std::string& GetName() const = 0;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_I_IDENTIFIABLE_H
