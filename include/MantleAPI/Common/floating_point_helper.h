/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  floating_point_helper.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_FLOATING_POINT_HELPER_H
#define MANTLEAPI_COMMON_FLOATING_POINT_HELPER_H

#include <units.h>

#include <cmath>
#include <limits>
#include <type_traits>

#if !defined(MANTLE_API_DEFAULT_EPS)
/// @brief Default epsilon for floating-point comparison.
///
/// > The C++ header `<limits>` provides a constant `std::numeric_limits<float>::epsilon()`
/// > for this purpose. The “standard” epsilon has the value 1.192092896e-07. This is
/// > to [_sic_] small for the problem at hand, where the error is roughly 5.0e-06. Just do the
/// > simplest thing that could work and use 1.0e-05 for epsilon. [1]
///
/// @par References
/// [1] Stubert, B. (2019) Comparing two floating-point numbers, Embedded Use. Available at: https://embeddeduse.com/2019/08/26/qt-compare-two-floats/ (Accessed: 10 May 2023).
///
#define MANTLE_API_DEFAULT_EPS 1.0e-05
#endif

namespace mantle_api
{

namespace details
{

/// @brief The signum function, that returns the sign of a real number
///
/// @tparam T the value type
/// @param[in] value  Real number
/// @returns sign of a real number
template <typename T, std::enable_if_t<std::is_arithmetic_v<T>, bool> = true>
constexpr int signum(const T value) noexcept
{
  return (T{0} < value) - (value < T{0});
}

}  // namespace details

/// @brief Default epsilon for floating-point comparison.
///
inline constexpr auto kDefaultEps = MANTLE_API_DEFAULT_EPS;

/// @brief Compare two values for almost-equality.
///
/// @tparam T the value type
/// @param[in] lhs The left-hand side value
/// @param[in] rhs The right-hand side value
/// @param[in] epsilon The epsilon for floating-point comparison. Defaults to kDefaultEps.
/// @param[in] absolute_comparison_only True if only absolute comparison should be used to test large numbers' equality. Defaults to false.
/// @returns  true if the value of lhs almost equal to the value of rhs.
template <typename T, std::enable_if_t<std::is_floating_point_v<T>, bool> = true>
constexpr bool AlmostEqual(const T lhs, const T rhs, const T epsilon = static_cast<T>(kDefaultEps), bool absolute_comparison_only = false) noexcept
{
  // Handle NaN.
  // NaN should never be equal to anything.
  if (std::isnan(lhs) || std::isnan(rhs))
  {
    return false;
  }

  // Handle infinity.
  // Infinity should only ever be equal to infinity with the same signum.
  if (std::isinf(lhs) != std::isinf(rhs))
  {
    return false;
  }

  if (std::isinf(lhs) && std::isinf(rhs))
  {
    return details::signum(lhs) == details::signum(rhs);
  }

  // Handle small values.
  // Values are considered almost equal, if their difference is less than epsilon.
  if (std::abs(lhs - rhs) <= epsilon)
  {
    return true;
  }

  // Handle bigger values.
  // Values are considered almost equal, if their difference is less than epsilon multiplied by the bigger absolute value.
  if(!absolute_comparison_only)
  {
    return std::abs(lhs - rhs) <= epsilon * std::max(std::abs(lhs), std::abs(rhs));
  }

  return false;
}

/// @brief Compare two values for greater-or-almost-equality.
///
/// @tparam T the value type
/// @param[in] lhs The left-hand side value
/// @param[in] rhs The right-hand side value
/// @param[in] epsilon The epsilon for floating-point comparison. Defaults to kDefaultEps.
/// @param[in] absolute_comparison_only True if only absolute comparison should be used to test large numbers' equality. Defaults to false.
/// @returns  true if the value of lhs greater or almost equal to the value of rhs.
template <typename T, std::enable_if_t<std::is_floating_point_v<T>, bool> = true>
constexpr bool GreaterOrEqual(const T lhs, const T rhs, const T epsilon = static_cast<T>(kDefaultEps), bool absolute_comparison_only = false) noexcept
{
  if (lhs > rhs)
  {
    return true;
  }

  return AlmostEqual(lhs, rhs, epsilon, absolute_comparison_only);
}

/// @brief Compare two values for less-or-almost-equality.
///
/// @tparam T the value type
/// @param[in] lhs The left-hand side value
/// @param[in] rhs The right-hand side value
/// @param[in] epsilon The epsilon for floating-point comparison. Defaults to kDefaultEps.
/// @param[in] absolute_comparison_only True if only absolute comparison should be used to test large numbers' equality. Defaults to false.
/// @returns  true if the value of lhs less or almost equal to the value of rhs.
template <typename T, std::enable_if_t<std::is_floating_point_v<T>, bool> = true>
constexpr bool LessOrEqual(const T lhs, const T rhs, const T epsilon = static_cast<T>(kDefaultEps), bool absolute_comparison_only = false) noexcept
{
  if (lhs < rhs)
  {
    return true;
  }

  return AlmostEqual(lhs, rhs, epsilon, absolute_comparison_only);
}

/// @brief Compare two values for almost-equality.
///
/// @tparam T the value type
/// @param[in] lhs The left-hand side value
/// @param[in] rhs The right-hand side value
/// @param[in] epsilon The epsilon for floating-point comparison. Defaults to kDefaultEps.
/// @param[in] absolute_comparison_only True if only absolute comparison should be used to test large numbers' equality. Defaults to false.
/// @returns  true if the value of lhs almost equal to the value of rhs.
template <typename T, std::enable_if_t<units::traits::is_unit_t<T>::value, bool> = true>
constexpr bool AlmostEqual(const T lhs, const T rhs, const T epsilon = T{kDefaultEps}, bool absolute_comparison_only = false) noexcept
{
  return AlmostEqual(lhs(), rhs(), epsilon(), absolute_comparison_only);
}

/// @brief Compare two values for greater-or-almost-equality.
///
/// @tparam T the value type
/// @param[in] lhs The left-hand side value
/// @param[in] rhs The right-hand side value
/// @param[in] epsilon The epsilon for floating-point comparison. Defaults to kDefaultEps.
/// @param[in] absolute_comparison_only True if only absolute comparison should be used to test large numbers' equality. Defaults to false.
/// @returns  true if the value of lhs greater or almost equal to the value of rhs.
template <typename T, std::enable_if_t<units::traits::is_unit_t<T>::value, bool> = true>
constexpr bool GreaterOrEqual(const T lhs, const T rhs, const T epsilon = T{kDefaultEps}, bool absolute_comparison_only = false) noexcept
{
  return GreaterOrEqual(lhs(), rhs(), epsilon(), absolute_comparison_only);
}

/// @brief Compare two values for less-or-almost-equality.
///
/// @tparam T the value type
/// @param[in] lhs The left-hand side value
/// @param[in] rhs The right-hand side value
/// @param[in] epsilon The epsilon for floating-point comparison. Defaults to kDefaultEps.
/// @param[in] absolute_comparison_only True if only absolute comparison should be used to test large numbers' equality. Defaults to false.
/// @returns  true if the value of lhs less or almost equal to the value of rhs.
template <typename T, std::enable_if_t<units::traits::is_unit_t<T>::value, bool> = true>
constexpr bool LessOrEqual(const T lhs, const T rhs, const T epsilon = T{kDefaultEps}, bool absolute_comparison_only = false) noexcept
{
  return LessOrEqual(lhs(), rhs(), epsilon(), absolute_comparison_only);
}

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_FLOATING_POINT_HELPER_H
