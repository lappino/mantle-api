/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  unit_definitions.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_UNIT_DEFINITIONS_H
#define MANTLEAPI_COMMON_UNIT_DEFINITIONS_H

#include <units.h>

namespace units
{

/// Adds a single new unit to the given namespace, as well as a literal definition and `cout` support based on the given
/// `abbreviation`.
UNIT_ADD(angular_acceleration,
         radians_per_second_squared,
         radians_per_second_squared,
         rad_per_s_sq,
         compound_unit<angle::radians, inverse<squared<time::seconds>>>)

namespace category
{
using angular_acceleration_unit = base_unit<detail::meter_ratio<0>, std::ratio<0>, std::ratio<-2>, std::ratio<1>>;
}  // namespace category

UNIT_ADD_CATEGORY_TRAIT(angular_acceleration)

/// Adds a single new unit to the given namespace, as well as a literal definition and `cout` support based on the given
/// `abbreviation`.
UNIT_ADD(angular_jerk,
         radians_per_second_cubed,
         radians_per_second_cubed,
         rad_per_s_cu,
         compound_unit<angle::radians, inverse<cubed<time::seconds>>>)

namespace category
{
using angular_jerk_unit = base_unit<detail::meter_ratio<0>, std::ratio<0>, std::ratio<-3>, std::ratio<1>>;
}  // namespace category

UNIT_ADD_CATEGORY_TRAIT(angular_jerk)

/// Adds unit 'jerk' to the given namespace, as well as a literal definition and `cout` support based on the given
/// `abbreviation`.
UNIT_ADD(jerk,
         meters_per_second_cubed,
         meters_per_second_cubed,
         mps_cu,
         compound_unit<velocity::meters_per_second, inverse<squared<time::seconds>>>)

namespace category
{

using jerk_unit = base_unit<detail::meter_ratio<1>, std::ratio<0>, std::ratio<-3>>;

}  // namespace category

UNIT_ADD_CATEGORY_TRAIT(jerk)

/// Adds new unit 'jerk_acceleration' to the given namespace, as well as a literal definition and `cout` support based on the given
/// `abbreviation`.
UNIT_ADD(jerk_acceleration,
         meters_per_second_to_the_power_of_four,
         meters_per_second_to_the_power_of_four,
         mps_pow4,
         compound_unit<velocity::meters_per_second, inverse<cubed<time::seconds>>>)

namespace category
{

using jerk_acceleration_unit = base_unit<detail::meter_ratio<1>, std::ratio<0>, std::ratio<-4>>;

}  // namespace category

UNIT_ADD_CATEGORY_TRAIT(jerk_acceleration)

}  // namespace units

#endif  // MANTLEAPI_COMMON_UNIT_DEFINITIONS_H
