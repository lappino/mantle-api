/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  dimension.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_DIMENSION_H
#define MANTLEAPI_COMMON_DIMENSION_H

#include <MantleAPI/Common/floating_point_helper.h>
#include <units.h>

namespace mantle_api
{

/// This struct represents the dimension of the bounding box
struct Dimension3
{
  units::length::meter_t length{0};  ///< Length of the object’s bounding box
  units::length::meter_t width{0};   ///< Width of the object’s bounding box
  units::length::meter_t height{0};  ///< Height of the object’s bounding box
};

/// @brief  almost-equality
/// @details  Compares the values of two dimensions.
/// @param[in]  lhs The left-hand side value for the comparison
/// @param[in]	rhs The right-hand side value for the comparison
/// @returns  true if the values of lhs almost equal to the values of rhs.
constexpr bool operator==(const Dimension3& lhs, const Dimension3& rhs) noexcept
{
  return AlmostEqual(lhs.length, rhs.length) && AlmostEqual(lhs.width, rhs.width) && AlmostEqual(lhs.height, rhs.height);
}

/// @brief  inequality
/// @details  Compares the value of two dimensions.
/// @param[in]  lhs left-hand side value for the comparison
/// @param[in]	rhs right-hand side value for the comparison
/// @returns  true if the value of lhs is not almost equal to the value of rhs.
constexpr bool operator!=(const Dimension3& lhs, const Dimension3& rhs) noexcept
{
  return !(lhs == rhs);
}

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_DIMENSION_H
