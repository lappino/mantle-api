/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MANTLEAPI_COMMON_LOG_UTILS_H
#define MANTLEAPI_COMMON_LOG_UTILS_H

#include <MantleAPI/Common/i_logger.h>

#include <array>
#include <cstddef>
#include <ostream>
#include <string_view>

namespace mantle_api
{

namespace log_utils
{

static inline constexpr auto kLogLevelNames = std::array<std::string_view, 6U>{"Trace", "Debug", "Info", "Warning", "Error", "Critical"};

[[nodiscard]] constexpr std::string_view ToStringView(LogLevel level) noexcept
{
  return (level >= LogLevel::kTrace && level <= LogLevel::kCritical) ? kLogLevelNames.at(static_cast<std::size_t>(level)) : "Log level out of range";
}

}  // namespace log_utils

inline std::ostream& operator<<(std::ostream& os, mantle_api::LogLevel level) noexcept
{
  os << log_utils::ToStringView(level);
  return os;
}

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_LOG_UTILS_H
