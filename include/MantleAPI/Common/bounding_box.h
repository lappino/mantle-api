/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  bounding_box.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_BOUNDING_BOX_H
#define MANTLEAPI_COMMON_BOUNDING_BOX_H

#include <MantleAPI/Common/dimension.h>
#include <MantleAPI/Common/vector.h>
#include <units.h>

namespace mantle_api
{

/// Each entity has its own local, right-handed coordinate system, where x means "forward" (e.g. driving direction), y means left and z means upwards.
/// The origin of the entity coordinate system is determined by specifying the offset of the geometric bounding box center in entity coordinates.
/// For vehicles the origin shall be the center of the rear axis.
struct BoundingBox
{
  Vec3<units::length::meter_t> geometric_center{}; ///< Coordinates of bounding box center in local coordinate system
  Dimension3 dimension{}; ///< Dimension of the bounding box (i.e. length = x dimension, width = y dimension, height = z dimension)
};

/// @brief  equality
/// @details  Compares the values of two BoundingBoxes.
/// @param[in]  lhs left-hand side value for the comparison
/// @param[in]	rhs right-hand side value for the comparison
/// @returns  true if the values of lhs exactly equal to the values of rhs.
constexpr bool operator==(const BoundingBox& lhs, const BoundingBox& rhs) noexcept
{
  return lhs.geometric_center == rhs.geometric_center &&
         lhs.dimension == rhs.dimension;
}

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_BOUNDING_BOX_H
