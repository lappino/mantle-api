/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  weather.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_ENVIRONMENTALCONDITIONS_WEATHER_H
#define MANTLEAPI_ENVIRONMENTALCONDITIONS_WEATHER_H

#include <units.h>

namespace mantle_api
{
/// Specify the amount of the precipitation.
enum class Precipitation
{
  kUnknown,
  kOther,
  kNone,
  kVeryLight,
  kLight,
  kModerate,
  kHeavy,
  kVeryHeavy,
  kExtreme
};

/// Specify the amount of the fog.
enum class Fog
{
  kUnknown,
  kOther,
  kExcellentVisibility,
  kGoodVisibility,
  kModerateVisibility,
  kPoorVisibility,
  kMist,
  kLight,
  kThick,
  kDense
};

/// Specify the level of illumination.
enum class Illumination
{
  kUnknown,
  kOther,
  kLevel1,
  kLevel2,
  kLevel3,
  kLevel4,
  kLevel5,
  kLevel6,
  kLevel7,
  kLevel8,
  kLevel9
};

/// Definition of weather conditions in terms of fog, precipitation, illumination, humidity, temperature and atmospheric pressure states
struct Weather
{
  Fog fog{Fog::kExcellentVisibility};                   ///< Defines the fog, i.e. visual range
  Precipitation precipitation{Precipitation::kNone};    ///< Defines the precipitation, i.e. intensity
  Illumination illumination{Illumination::kOther};      ///< Defines the illumination using levels
  units::concentration::percent_t humidity{0.0};        ///< Defines the humidity
  units::temperature::kelvin_t temperature{0.0};        ///< Defines the outside temperature around the entities of the scenario
  units::pressure::pascal_t atmospheric_pressure{0.0};  ///< Defines the atmospheric pressure around the entities of the scenario
};

}  // namespace mantle_api

#endif  // MANTLEAPI_ENVIRONMENTALCONDITIONS_WEATHER_H
