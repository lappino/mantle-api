/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  map_details.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_MAP_MAP_DETAILS_H
#define MANTLEAPI_MAP_MAP_DETAILS_H

#include <MantleAPI/Common/position.h>

#include <memory>
#include <vector>

namespace mantle_api
{
/// Definition of the map area
struct MapDetails
{
  virtual ~MapDetails() = default;

  /// Cloning MapDetails for usage as unique ptr in ScenarioInfo. Can be overwritten in derived classes.
  /// @return Copy of MapDetails
  virtual std::unique_ptr<MapDetails> Clone() const
  {
    return std::make_unique<MapDetails>(*this);
  }

  /// Area of the map (e.g. GPS latitude, GPS longitude)
  std::vector<Position> map_region;
};

}  // namespace mantle_api
#endif  // MANTLEAPI_MAP_MAP_DETAILS_H
