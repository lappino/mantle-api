/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_traffic_swarm_service.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_TRAFFIC_I_TRAFFIC_SWARM_CONTROLLER_H
#define MANTLEAPI_TRAFFIC_I_TRAFFIC_SWARM_CONTROLLER_H

#include <vector>
#include <MantleAPI/Common/vector.h>
#include <MantleAPI/Common/pose.h>
#include <units.h>

namespace mantle_api
{

/// This struct represents the parameters of the swarm traffic
struct TrafficSwarmParameters
{
    /// The range for the lower and upper limit of the entity's starting speeds
    struct SpeedRange
    {
        units::velocity::meters_per_second_t minimum;   ///< The lower limit of a speed range
        units::velocity::meters_per_second_t maximum;   ///< The upper limit of a speed range
    };

    /// Name of the central entity the swarm traffic is created around
    std::string central_entity_name;
    /// The maximum number of vehicles surrounding the central entity
    size_t maximum_number_of_vehicles;
    /// The starting speeds of the spawned entities
    SpeedRange speed_range;
    /// Radius of the inner circular area around the central entity
    units::length::meter_t exclusion_radius;
    /// Shape of the swarm traffic distribution area is given as an ellipsis around a central entity. 
    /// semi_minor_spawning_radius defines the half length of the minor axis of this ellipsis.
    units::length::meter_t semi_minor_spawning_radius;
    // Shape of the swarm traffic distribution area is given as an ellipsis around a central entity. 
    /// semi_major_spawning_radius defines the half length of the major axis of this ellipsis.
    units::length::meter_t semi_major_spawning_radius;
    /// Offset in longitudinal direction related to the x-axis of the central entity
    units::length::meter_t spawning_area_longitudinal_offset;
};

/// Base interface for the swarm traffic
class ITrafficSwarmService
{
public:
    
    /// Specify the relative position of the spawned vehicle to the central entity
    enum class RelativePosition
    {
        kInFront = 0,
        kBehind
    };

    /// Specify the position of the spawned vehicle
    using SpawningPosition = std::pair<mantle_api::Pose, RelativePosition>;

    /// Returns available spawning poses
    ///
    /// @return list of poses
    virtual std::vector<SpawningPosition> GetAvailableSpawningPoses() const = 0;

    /// Returns additional properties for entity objects of type vehicle
    ///
    /// @param vehicle_class Category of the vehicle (car, bicycle, train,...)
    /// @return vehicle properties
    virtual mantle_api::VehicleProperties GetVehicleProperties(mantle_api::VehicleClass vehicle_class) const = 0;

    /// Updates parameters for the controller
    ///
    /// @param config External controller configuration file
    /// @param speed  The speed of the environment controller
    virtual void UpdateControllerConfig(std::unique_ptr<mantle_api::ExternalControllerConfig>& config,
                                        units::velocity::meters_per_second_t speed) = 0;
    /// Sets the number of swarm entities
    ///
    /// @param count number of swarm entities
    virtual void SetSwarmEntitiesCount(size_t count) = 0;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_TRAFFIC_I_TRAFFIC_SWARM_CONTROLLER_H
