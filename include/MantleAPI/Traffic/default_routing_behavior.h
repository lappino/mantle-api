/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MANTLEAPI_TRAFFIC_DEFAULT_ROUTING_BEHAVIOR_H
#define MANTLEAPI_TRAFFIC_DEFAULT_ROUTING_BEHAVIOR_H

namespace mantle_api {

/// Specify behavior to end of route
enum class DefaultRoutingBehavior
{
    kStop,          ///< Do nothing
    kRandomRoute    ///< Randomly select where to go next
};

}  // namespace mantle_api

#endif  // MANTLEAPI_TRAFFIC_DEFAULT_ROUTING_BEHAVIOR_H