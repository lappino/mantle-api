/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_entity.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_TRAFFIC_I_ENTITY_H
#define MANTLEAPI_TRAFFIC_I_ENTITY_H

#include <MantleAPI/Common/bounding_box.h>
#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Traffic/entity_properties.h>
#include <MantleAPI/Traffic/i_controller_config.h>

#include <memory>
#include <vector>

namespace mantle_api
{

/// Definition of the visibility of the scenario entity
struct EntityVisibilityConfig
{
  /// The "graphics" flag determines, if the entity shall be shown in visualizations of the simulated environment
  bool graphics{true};
  /// The "traffic" flag determines, if the entity is contained in the common simulator output (e.g. OSI Ground Truth)
  /// for all traffic participants
  bool traffic{true};
  /// The "sensors" flag can be used to simulate sensor errors (false positives / false negatives). For this the
  /// "sensors" flag value has to diverge from the "traffic" flag value (e.g. false positive: traffic=false,
  /// sensors=true)
  bool sensors{true};
  /// - Sensor errors can be specified for single sensors only. E.g. a false positive for a radar sensor would be
  /// specified with traffic="false", sensors="true", sensor_names={"radar"}.
  /// - For the other not named sensors implicitly the opposite applies. So in the above example e.g. for a camera
  /// sensor the entity would be not visible / not contained in its OSI Sensor View.
  /// - When "sensor_names" is not specified (empty vector) then the "sensors" flag applies to
  /// all sensors.
  std::vector<std::string> sensor_names{};
};

/// Base interface for all static and dynamic scenario entities.
class IEntity : public virtual IIdentifiable
{
public:
  /// @brief Sets the position of the scenario entity
  ///
  /// @details The position of the entity is the geometric center of its bounding box. The origin of the entity coordinate system
  ///          can be defined flexibly in relation to the geometric center (see bounding box).
  /// @param inert_pos  Position of the scenario entity
  virtual void SetPosition(const Vec3<units::length::meter_t>& inert_pos) = 0;

  /// @brief Gets the position of the scenario entity
  ///
  /// @return Position of the scenario entity
  [[nodiscard]] virtual Vec3<units::length::meter_t> GetPosition() const = 0;

  /// @brief Sets the velocity of the scenario entity
  ///
  /// @param velocity Velocity vector (Forward, Sideward, Upward)
  virtual void SetVelocity(const Vec3<units::velocity::meters_per_second_t>& velocity) = 0;

  /// @brief Gets the velocity of the scenario entity
  ///
  /// @return Velocity vector (Forward, Sideward, Upward)
  [[nodiscard]] virtual Vec3<units::velocity::meters_per_second_t> GetVelocity() const = 0;

  /// @brief Sets the acceleration of the scenario entity
  ///
  /// @param acceleration Acceleration vector (Forward, Sideward, Upward)
  virtual void SetAcceleration(const Vec3<units::acceleration::meters_per_second_squared_t>& acceleration) = 0;

  /// @brief Gets the acceleration of the scenario entity
  ///
  /// @return Acceleration vector (Forward, Sideward, Upward)
  [[nodiscard]] virtual Vec3<units::acceleration::meters_per_second_squared_t> GetAcceleration() const = 0;

  /// @brief Sets the orientation of the scenario entity
  ///
  /// @param orientation 3D representation of orientation using yaw, pitch, and roll angles
  virtual void SetOrientation(const Orientation3<units::angle::radian_t>& orientation) = 0;

  /// @brief Gets the orientation of the scenario entity
  ///
  /// @return 3D representation of orientation using yaw, pitch, and roll angles
  [[nodiscard]] virtual Orientation3<units::angle::radian_t> GetOrientation() const = 0;

  /// @brief Sets the orientation rate (angular velocity) of the scenario entity
  ///
  /// @param orientation_rate Rate of change of a scenario entity's orientation over time
  virtual void SetOrientationRate(
      const Orientation3<units::angular_velocity::radians_per_second_t>& orientation_rate) = 0;

  /// @brief Gets the orientation rate (angular velocity) of the scenario entity
  ///
  /// @return Rate at which yaw, pitch, and roll angles are changing with respect to time
  [[nodiscard]] virtual Orientation3<units::angular_velocity::radians_per_second_t> GetOrientationRate() const = 0;

  /// @brief Sets the orientation acceleration (angular acceleration) of the scenario entity
  ///
  /// @param orientation_acceleration Rate of change of a scenario entity's orientation rate with respect to time
  virtual void SetOrientationAcceleration(
      const Orientation3<units::angular_acceleration::radians_per_second_squared_t>& orientation_acceleration) = 0;

  /// @brief Gets the orientation acceleration (angular acceleration) of the scenario entity
  ///
  /// @return Orientation acceleration (angular acceleration)
  [[nodiscard]] virtual Orientation3<units::angular_acceleration::radians_per_second_squared_t> GetOrientationAcceleration()
      const = 0;

  /// @brief Sets the properties that describe scenario entity
  ///
  /// @param properties Basic properties that describe scenario entity
  virtual void SetProperties(std::unique_ptr<mantle_api::EntityProperties> properties) = 0;

  /// @brief Gets the properties that describe scenario entity
  ///
  /// @return Basic properties that describe scenario entity
  [[nodiscard]] virtual EntityProperties* GetProperties() const = 0;

  /// @brief Sets the IDs of the lanes that scenario entity is assigned to
  ///
  /// @param assigned_lane_ids The IDs of the lanes that scenario entity is assigned to
  virtual void SetAssignedLaneIds(const std::vector<std::uint64_t>& assigned_lane_ids) = 0;

  /// @brief Gets the IDs of the lanes that scenario entity is assigned to
  ///
  /// @return IDs of the lanes that scenario entity is assigned to
  [[nodiscard]] virtual std::vector<std::uint64_t> GetAssignedLaneIds() const = 0;

  /// @brief Sets the visibility of the scenario entity
  ///
  /// @param visibility Visibility of the scenario entity
  virtual void SetVisibility(const EntityVisibilityConfig& visibility) = 0;

  /// @brief Gets the visibility of the scenario entity
  ///
  /// @return Visibility of the scenario entity
  [[nodiscard]] virtual EntityVisibilityConfig GetVisibility() const = 0;
};

/// Interface for dynamic scenario entities of vehicle type
class IVehicle : public virtual IEntity
{
public:
  /// @brief Gets the additional properties for entity object of type vehicle
  ///
  /// @return Additional properties that describe scenario entity object of type vehicle
  [[nodiscard]] VehicleProperties* GetProperties() const override = 0;

  /// @brief Set the indicator's state
  ///
  /// @param state State of the indicator
  virtual void SetIndicatorState(IndicatorState state) = 0;

  /// @brief Gets the indicator's state
  ///
  /// @return State of the indicator
  [[nodiscard]] virtual IndicatorState GetIndicatorState() const = 0;

  //    virtual bool IsHost() const = 0;
  //    virtual void SetHost() = 0;
};

/// Interface for scenario entities of pedestrian type
class IPedestrian : public virtual IEntity
{
public:
  /// @brief Gets the additional properties for entity object of type pedestrian
  ///
  /// @return Additional properties that describe scenario entity object of type pedestrian
  [[nodiscard]] PedestrianProperties* GetProperties() const override = 0;
};

/// Interface for static scenario entities
class IStaticObject : public virtual IEntity
{
public:
  /// @brief Gets the additional properties for static scenario entity
  ///
  /// @return Additional properties that describe static scenario entity
  [[nodiscard]] StaticObjectProperties* GetProperties() const override = 0;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_TRAFFIC_I_ENTITY_H
