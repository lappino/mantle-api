/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  scenario_info.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_EXECUTION_SCENARIO_INFO_H
#define MANTLEAPI_EXECUTION_SCENARIO_INFO_H

#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Map/map_details.h>

#include <map>
#include <memory>
#include <string>

namespace mantle_api
{
/// Information about the scenario
struct ScenarioInfo
{
  /// Default constructor to avoid nullptr for map details
  ScenarioInfo() = default;

  /// Copy constructor
  /// @param other scenario info to copy
  ScenarioInfo(const ScenarioInfo& other)
      : scenario_timeout_duration{other.scenario_timeout_duration},
        description{other.description},
        full_map_path{other.full_map_path},
        map_details{other.map_details->Clone()},
        additional_information{other.additional_information}
  {
  }

  /// Move constructor
  /// @param other scenario info to move
  ScenarioInfo(ScenarioInfo&& other) noexcept = default;

  /// Copy assignment operator
  /// @param other scenario info to copy assign
  /// @return copied ScenarioInfo
  ScenarioInfo& operator=(const ScenarioInfo& other)
  {
    if (this != &other)
    {
      scenario_timeout_duration = other.scenario_timeout_duration;
      description = other.description;
      full_map_path = other.full_map_path;
      map_details = other.map_details->Clone();
      additional_information = other.additional_information;
    }
    return *this;
  }

  /// Move assignment operator
  /// @param other scenario info to move assign
  /// @return moved ScenarioInfo
  ScenarioInfo& operator=(ScenarioInfo&& other) noexcept = default;

  ~ScenarioInfo() = default;

  /// Duration of the scenario timeout
  Time scenario_timeout_duration;
  /// Specific description of the scenario
  std::string description;
  /// Absolute path to the map
  std::string full_map_path;
  /// Definition of the map area
  std::unique_ptr<MapDetails> map_details{std::make_unique<MapDetails>()};
  /// Additional custom information about the scenario
  std::map<std::string, std::string> additional_information;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_EXECUTION_SCENARIO_INFO_H
